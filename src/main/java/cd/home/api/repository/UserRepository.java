package cd.home.api.repository;

import cd.home.api.entitiy.Topic;
import cd.home.api.entitiy.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUserName(String userName);
}
