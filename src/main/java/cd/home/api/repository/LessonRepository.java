package cd.home.api.repository;

import cd.home.api.entitiy.Lesson;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LessonRepository extends CrudRepository<Lesson,String> {
    public List<Lesson> findCourseById(String courseId);

}
