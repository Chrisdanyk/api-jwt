package cd.home.api.repository;

import cd.home.api.entitiy.Topic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public interface TopicRepository extends CrudRepository<Topic,String> {

}
